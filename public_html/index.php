<?php
/**
 * Created by PhpStorm.
 * Auther @ Mads Roloff - Rights reservede to Author
 * Date: 03-04-2017
 */ ?>
<?php
require_once filter_input(INPUT_SERVER, "DOCUMENT_ROOT") . "/assets/incl/init.php";

require_once filter_input(INPUT_SERVER, "DOCUMENT_ROOT") . "/assets/incl/nav.php";


$product = new shopproduct();

$mode = setMode();

switch (strtoupper($mode)) {


    default:

        $prod = $product->getAllLimit(2);
        sysHeader();
        ?>
        <!-- Page Content -->
        <div class="container">

            <div class="row">

                <div class="col-md-3">
                    <p class="lead">Webshop</p>
                    <div class="list-group">
                        <a href="#" class="list-group-item">Bukser</a>
                        <a href="#" class="list-group-item">Trøjer</a>
                        <a href="#" class="list-group-item">T-shirts</a>
                    </div>
                </div>
                <div class="col-md-9">
                    <div class="row">
                        <div id="comments">
                            <?php foreach ($prod

                            as $key => $row): ?>
                            <a href="<?php echo "mode?DETAILS?&ProductID=" . $row["iProductID"] ?>">

                                <div class="col-sm-4 col-lg-4 col-md-4 id="<?php echo "product" . $row["iProductID"] ?>">
                                <div class="thumbnail">
                                    <img src="http://placehold.it/320x150" alt="">
                                    <div class="caption">
                                        <h4 class="pull-right"><?php echo $row["iPrice"] ?>,-</h4>
                                        <h4>
                                            <a href="<?php echo "?mode=Details&iProductID=" . $row["iProductID"] ?>"><?php echo $row["vcTitle"] ?></a>
                                        </h4>
                                        <p><?php echo substr($row["txDescShort"], 0, 35); ?></p>


                                        <!--  <div class="col-md-12">


                                            <a href="<?php // echo $product->iProductID
                                        ?>" class="btn btn-info addtocart pull-right"
                                               data-productid=""> Add to
                                                cart</a>
                                            <input style="width: 50%;" class="form-control quantity pull-right" value="1" type="number" name="quantity">


                                            <div class="isincart"></div>
                                        </div>
                                            -->

                                    </div>
                                    <div class="ratings">
                                        <p class="pull-right">15 reviews</p>
                                        <p>
                                            <span class="glyphicon glyphicon-star"></span>
                                            <span class="glyphicon glyphicon-star"></span>
                                            <span class="glyphicon glyphicon-star"></span>
                                            <span class="glyphicon glyphicon-star"></span>
                                            <span class="glyphicon glyphicon-star"></span>
                                        </p>
                                    </div>
                                </div>
                        </div>
                        </a>

                        <?php endforeach; ?>
                    </div>
                </div>
                <button id="showmore">show more Products</button>
            </div>

        </div>
        <?php
        sysFooter();

        break;

    case "DETAILS":
        $iProductID = Filter_input(INPUT_GET, "iProductID", FILTER_SANITIZE_NUMBER_INT);
        $iParentID = Filter_input(INPUT_GET, "iParentID", FILTER_SANITIZE_NUMBER_INT);


        $product->getItem($iProductID);

        sysHeader();
        ?>


        <style>
            .product {
                border: 1px solid #dddddd;
                height: 321px;
            }

            .product > img {
                max-width: 350px;
                height: 320px;
                width: 350px;
            }

            .product-rating {
                font-size: 20px;
                margin-bottom: 25px;
            }

            .product-title {
                font-size: 20px;
            }

            .product-desc {
                font-size: 14px;
            }

            .product-price {
                font-size: 22px;
            }

            .product-stock {
                color: #74DF00;
                font-size: 20px;
                margin-top: 10px;
            }

            .product-info {
                margin-top: 50px;
            }

            /*********************************************
                                VIEW
            *********************************************/

            .content-wrapper {
                max-width: 1140px;
                background: #fff;
                margin: 0 auto;
                margin-top: 25px;
                margin-bottom: 10px;
                border: 0px;
                border-radius: 0px;
            }

            .container {
                padding-left: 0px;
                padding-right: 0px;
                max-width: 100%;
            }

            /*********************************************
                            ITEM
            *********************************************/

            .service1-items {
                padding: 0px 0 0px 0;
                float: left;
                position: relative;
                overflow: hidden;
                max-width: 100%;
                height: 321px;
                width: 130px;
            }

            .service1-item {
                height: 107px;
                width: 120px;
                display: block;
                float: left;
                position: relative;
                padding-right: 20px;
                border-right: 1px solid #DDD;
                border-top: 1px solid #DDD;
                border-bottom: 1px solid #DDD;
            }

            .service1-item > img {
                max-height: 110px;
                max-width: 110px;
                opacity: 0.6;
                transition: all .2s ease-in;
                -o-transition: all .2s ease-in;
                -moz-transition: all .2s ease-in;
                -webkit-transition: all .2s ease-in;
            }

            .service1-item > img:hover {
                cursor: pointer;
                opacity: 1;
            }

            .service-image-left {
                padding-right: 50px;
            }

            .service-image-right {
                padding-left: 50px;
            }

            .service-image-left > center > img, .service-image-right > center > img {
                max-height: 155px;
            }

        </style>
        <div>
        <div class="container">

        <div class="row">
        <!--
                    <div class="col-md-3">
                        <p class="lead">Shop Name</p>
                        <div class="list-group">
                            <a href="#" class="list-group-item active">Category 1</a>
                            <a href="#" class="list-group-item">Category 2</a>
                            <a href="#" class="list-group-item">Category 3</a>
                        </div>
                    </div>
                    -->
        <div class="content-wrapper">
            <div class="item-container">
                <div class="container">
                    <div class="col-md-6">
                        <div class="product col-md-8 service-image-left">


                            <img id="item-display" src="<?php echo $product->vcImage1 ?>" alt=""></img>

                        </div>

                        <div class="container service1-items col-sm-2 col-md-2 pull-left">

                            <a id="item-1" class="service1-item">
                                <img src="<?php echo $product->vcImage1 ?>" alt=""></img>
                            </a>
                            <a id="item-2" class="service1-item">
                                <img src="<?php echo $product->vcImage2 ?>" alt=""></img>
                            </a>
                            <a id="item-3" class="service1-item">
                                <img src="<?php echo $product->vcImage3 ?>" alt=""></img>
                            </a>

                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="product-title"><?php echo $product->vcTitle ?></div>
                        <div class="product-desc"><?php echo $product->txDescShort ?></div>
                        <div class="product-rating"><i class="fa fa-star gold"></i> <i class="fa fa-star gold"></i> <i class="fa fa-star gold"></i> <i class="fa fa-star gold"></i>
                            <i class="fa fa-star-o"></i></div>
                        <hr>
                        <div class="product-price"><?php echo $product->iPrice ?>,-</div>
                        <div class="product-stock"><?php if ($product->iStock > 0) {
                                echo "Varen er på lager";
                            } else {
                                "Varen er udsolgt";
                            } ?></div>
                        <hr>
                        <div class="col-md-6">
                            <a style="margin-bottom: 10px;width: 100%;" class="btn btn-info addtocart pull-right"
                               data-productid="<?php echo $product->iProductID ?>"> Add to
                                cart</a>
                            <input id="quantity-<?php echo $product->iProductID ?>" class="form-control quantity pull-right" value="1" type="number" name="quantity">

                            <div class="isincart"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container-fluid">
                <div class="col-md-12 product-info">
                    <ul id="myTab" class="nav nav-tabs nav_tabs">

                        <li class="active"><a href="#service-one" data-toggle="tab">DESCRIPTION</a></li>
                        <li><a href="#service-three" data-toggle="tab">REVIEWS</a></li>

                    </ul>
                    <div id="myTabContent" class="tab-content">
                        <div class="tab-pane fade in active" id="service-one">

                            <section class="container product-info">

                                <?php echo $product->txDesc ?>

                                <h3><?php echo $product->vcTitle ?> Spefikationer:</h3>
                                <li>Vægt: <?php echo $product->iWeight ?> kg</li>
                                <li>Enter Facts here</li>
                            </section>

                        </div>
                        <div class="tab-pane fade" id="service-two">

                            <section class="container">

                            </section>

                        </div>
                        <div class="tab-pane fade" id="service-three">

                        </div>
                    </div>
                    <hr>
                </div>
            </div>

        </div>


        <div class="container">

            <hr>

            <!-- Footer -->
            <footer>
                <div class="row">
                    <div class="col-lg-12">
                        <p>Copyright &copy; Your Website 2014</p>
                    </div>
                </div>
            </footer>

        </div>
        <?php
        sysFooter();

        break;
}
