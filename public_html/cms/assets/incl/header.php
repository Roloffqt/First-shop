<!DOCTYPE html>

<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Admin</title>
    <link rel="icon" type="image/png" href="images/favicon-cog.ico">
    <meta name="viewport" content="width=device-width" initial-scale="1"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>


    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <link href="/cms/assets/css/main.css" rel="stylesheet">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans|Raleway|Montserrat|Source+Sans+Pro' rel='stylesheet' type='text/css'>
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">

</head>
<body>
<div class="">
    <!-- Navigation -->
    <nav class="navbar navbar-default">
        <div class="container-fluid">
            <ul class="nav navbar-nav">
                <li><a href="/cms/index.php">Forside</a></li>
                <li><a href="/cms/moduels/user.php?mode=list">Brugere</a></li>
                <li><a href="/cms/moduels/shopcategory.php?mode=list">Shop Category</a></li>
                <li><a href="/cms/moduels/shopproduct.php?mode=list">Shop Product</a></li>
                <a style="padding: 15px;" href="?action=logout" class="right logout-btn btn btn-danger"> Log Ud</a>
            </ul>
        </div>
    </nav>
    <div class="container">


