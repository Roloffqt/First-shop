<?php
/**
 * Created by PhpStorm.
 * Auther @ Mads Roloff - Rights reservede to Author
 * Date: 03-04-2017
 */ ?>

<head>
    <meta charset="UTF-8">
    <title>Form Fun</title>
    <link rel="icon" type="image/png" href="images/favicon-cog.ico">
    <meta name="viewport" content="width=device-width" initial-scale="1"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link href="\cms\assets\css\main.css" rel="stylesheet">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans|Raleway|Montserrat|Source+Sans+Pro' rel='stylesheet' type='text/css'>
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
    <style>
        .required:after {
            content: " *";
            color: red;
        }
    </style>
</head>
<body class="container">

<div class="col-lg-4">
    <h2>Login</h2>
    <form id="loginform" method="post" autocomplete="off">

        <div class="form-group">
            <label for="username" class=""> Username</label>
            <input type="text" class="form-control" id="username" autocapitalize="none" name="username" placeholder="Indtast brugernavn" value="">
        </div>

        <div class="form-group">
            <label for="password" class=""> Password</label>
            <input type="password" class="form-control" id="password" name="password" placeholder="password" value="">
        </div>

        <div class="btn-group">
            <button type="submit" value="login" class="login-button"><i class="fa fa-chevron-right"></i></button>
        </div>

    </form>
</div>
<!--REGISTRATION FORM-->
<div class="col-lg-push-4 col-lg-4">
    <h2>Register</h2>
    <!--Main Form-->
    <div class="login-form-1">
        <form id="" class="" onfocus="validate(this.form)">
            <div class="form-group">
                <label for="username" class="required">Username</label>
                <input type="text" class="form-control" id="username" name="username" data-requried="1" placeholder="Username" data-validate="username">
            </div>
            <div class="form-group">
                <label for="password" class="required">Password</label>
                <input type="password" class="form-control" id="password" name="password" placeholder="Password" data-requried="1" data-validate="password">
            </div>
            <div class="form-group">
                <label for="password_confirm" class="required"> Password Confirm </label>
                <input type="password" class="form-control" id="password_confirm" name="password_confirm" placeholder="Confirm Password" data-requried="1" data-validate="passwordConfirm">
            </div>


            <div class="form-group">
                <label for="fullname" class="required"> Full Name </label>
                <input type="text" class="form-control" id="fullname" name="fullname" placeholder="Full Name" data-validate="fullname">
            </div>

            <div class="form-group">
                <label for="tel" class="required"> Telefon </label>
                <input type="text" class="form-control" id="tel" name="tel" placeholder="Telefon" data-requried="1" data-validate="Telefon">
            </div>

            <div class="form-group">
                <input type="radio" class="" name="gender" id="male">
                <label for="Male">Male</label>

                <input type="radio" class="" name="gender" id="female">
                <label for="Female">Female</label>
            </div>

            <div class="form-group">
                <input type="checkbox" class="" id="agree" name="agree" data-requried="1">
                <label for="agree"> I agree with <a href="#">Terms of Service</a></label>
            </div>

            <button type="button" onclick="" class="disabled btn btn-success"><i class="fa fa-check"></i></button>


        </form>
    </div>
    <!--end:Main Form-->
</div>

</div>
<script src="https://code.jquery.com/jquery-3.1.1.min.js" integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8=" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
<script src="https://use.fontawesome.com/45215596e7.js"></script>
<script src="/<?php echo $path ?>/public_html/assets/js/validate.js"></script>

</body>
</html>