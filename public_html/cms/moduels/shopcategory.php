<?php
require_once filter_input(INPUT_SERVER, "DOCUMENT_ROOT") . "/cms/assets/incl/init.php";
$mode = setMode();

$strModuleName = "Category";

switch (strtoupper($mode)) {

    case "LIST";
        $iParentID = filter_input(INPUT_GET, "iParentID", FILTER_SANITIZE_NUMBER_INT, getDefaultValue(-1));

        $strModuleMode = "Oversigt";
        sysBackendHeader("Webshop");
        $arrButtonPanel = array();
        $arrButtonPanel[] = getButtonLink("plus", "?mode=edit&iCategoryID=-1", "Opret ny side", "btn-success");
        echo textpresenter::presentMode($strModuleName, $strModuleMode, $arrButtonPanel);

        $category = new shopcategory();
        $rows = $category->listByParent($iParentID);
        $arrValues = array();

        foreach ($rows as $key => $row) {
            $row["opts"] = getIcon("?mode=edit&iCategoryID=" . $row["iCategoryID"], "pencil", "Rediger")
                . getIcon("?mode=details&iCategoryID=" . $row["iCategoryID"], "eye", "Se side")
                . getIcon("", "trash", "Slet side", "remove(" . $row["iCategoryID"] . ")");

            $arrValues[] = $row;
            $subrows = $category->listByParent($row["iCategoryID"]);
            foreach ($subrows as $subkey => $subrow) {
                $subrow["opts"] = getIcon("?mode=edit&iCategoryID=" . $subrow["iCategoryID"], "pencil", "Rediger")
                    . getIcon("?mode=details&iCategoryID=" . $subrow["iCategoryID"], "eye", "Se side")
                    . getIcon("", "trash", "Slet side", "remove(" . $subrow["iCategoryID"] . ")");
                $subrow["vcTitle"] = "&nbsp;&raquo;&nbsp;" . $subrow["vcTitle"];
                $arrValues[] = $subrow;
            }
        }
        $p = new listPresenter($category->arrLabels, $arrValues);
        echo $p->presentlist();

        sysBackendfooter("Webshop");
        break;

    case "DETAILS";
        $iCategoryID = filter_input(INPUT_GET, "iCategoryID", FILTER_SANITIZE_NUMBER_INT);
        $strModuleMode = "Detaljer";
        sysBackendHeader("Webshop");

        $arrButtonPanel = array();
        $arrButtonPanel[] = getButtonLink("table", "?mode=list", "Oversigt", "btn-primary");
        $arrButtonPanel[] = getButtonLink("pencil", "?mode=edit&iCategoryID=" . $iCategoryID, "Rediger side", "btn-success");
        echo textpresenter::presentMode($strModuleName, $strModuleMode, $arrButtonPanel);

        $category = new shopcategory();
        $category->getItem($iCategoryID);
        $category->arrValues["iIsActive"] = boolToIcon($category->arrValues["iIsActive"]);

        $p = new listpresenter($category->arrColumns, $category->arrValues);
        echo $p->presentdetails();

        sysBackendfooter("Webshop");
        break;

    case "EDIT";
        $iCategoryID = filter_input(INPUT_GET, "iCategoryID", FILTER_SANITIZE_NUMBER_INT);

        $strModuleMode = ($iCategoryID > 0) ? "Rediger" : "Opret ny side";
        sysBackendHeader("Webshop");

        $arrButtonPanel = array();
        $arrButtonPanel[] = getButtonLink("table", "?mode=list", "Oversigt", "btn-primary");
        echo textpresenter::presentMode($strModuleName, $strModuleMode, $arrButtonPanel);

        $category = new shopcategory();
        if ($iCategoryID > 0) {
            $category->getItem($iCategoryID);
        }

        $parentOpts = $category->listOptions();
        array_unshift($parentOpts, array("-1", "Ingen"));
        $category->arrValues["iParentID"] = SelectBox("iParentID", $parentOpts, $category->arrValues["iParentID"]);

        $form = new formpresenter($category->arrColumns, $category->arrValues);
        echo $form->presentform();

        sysBackendfooter("Webshop");
        break;

    case "SAVE":
        $obj = new shopcategory();
        $iCategoryID = $obj->save();
        header("Location: ?mode=details&iCategoryID=$iCategoryID");
        break;

    case "DELETE":
        $obj = new shopcategory();
        $id = filter_input(INPUT_GET, "id", FILTER_VALIDATE_INT);
        $obj->delete($id);
        header("Location: ?mode=list");
        break;


}
