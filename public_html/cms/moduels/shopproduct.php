<?php
require_once filter_input(INPUT_SERVER, "DOCUMENT_ROOT") . "/cms/assets/incl/init.php";
$mode = setMode();
$strModuleName = "Shop product";

switch (strtoupper($mode)) {

    case "LIST":
        $strModuleMode = "Oversight";
        sysbackendHeader();
        $product = new shopproduct();;
        $arrButtonPanel = array();
        $arrButtonPanel[] = getButtonLink("plus", "?mode=edit&iProductID=-1", "Opret Nyt", "");
        echo textPresenter::presentMode($strModuleName, $strModuleMode, $arrButtonPanel);

        $product = new shopproduct();;
        $rows = $product->getAll();

        foreach ($rows as $key => $row) {
            $rows[$key]["opts"] = getIcon("?mode=edit&iProductID=" . $row["iProductID"], "rocket", "Redigere") .
                getIcon("?mode=details&iProductID=" . $row["iProductID"], "globe", "Detajiler") .
                getIcon("", "trash", "Slet element", "remove(" . $row["iProductID"] . ")");
        }

        $p = new listPresenter($product->arrLabels, $rows);
        echo $p->presentlist();

        sysbackendfooter();
        break;

    case "DETAILS":
        sysbackendHeader();
        $iProductID = Filter_input(INPUT_GET, "iProductID", FILTER_SANITIZE_NUMBER_INT);
        $product = new shopproduct();;


        $strModuleMode = "Detaljer";
        sysbackendHeader();
        $arrButtonPanel = array();
        $arrButtonPanel[] = getButtonLink("plus", "?mode=list", "Oversigt", "btn");
        $arrButtonPanel[] = getButtonLink("plus", "?mode=edit&iProductID=" . $iProductID, "Rediger", "btn");

        echo textPresenter::presentMode($strModuleName, $strModuleMode, $arrButtonPanel);
        $product = new shopproduct();;
        $product->getItem($iProductID);

        $product->arrValues["daCreated"] = date("d. M Y", $product->arrValues["daCreated"]);
        $p = new listPresenter($product->arrColumns, $product->arrValues);
        echo $p->presentdetails();


        sysbackendfooter();
        break;
    case "EDIT":
        $iProductID = filter_input(INPUT_GET, "iProductID", FILTER_SANITIZE_NUMBER_INT);
        $strModuleMode = ($iProductID > 0) ? "Rediger produkt" : "Opret nyt produkt";
        sysBackendHeader();

        /* Set array button panel */
        $arrButtonPanel = array();
        if ($iProductID > 0) {
            $arrButtonPanel[] = getButton("button", "Detaljer", "getUrl('?mode=productdetails&iProductID=" . $iProductID . "')");
        }
        $arrButtonPanel[] = getButton("button", "Oversigt", "document.location.href='?mode=list'");

        /* Call static panel with title and button options */
        echo textPresenter::presentMode($strModuleName, $strModuleMode, $arrButtonPanel);

        $product = new shopproduct();

        /* Set formtype for productgroup input selector */
        $product->arrColumns["arrGroups"] = array(
            "Formtype" => crud::INPUT_CHECKBOXMULTI,
            "Label" => "Produktgruppe(r)"
        );

        if ($iProductID > 0) {
            $product->getitem($iProductID);
        } else {
            $product->arrValues["iSortNum"] = 0; //$product->getNewSortNum($iParentID);
            $product->arrValues["arrGroups"] = array();
        }

        /* Get all categories */
        $group = new shopcategory();
        $arrGroups = $group->getopts(-1);

        /* Build options array
         *  0 => int iGroupID
         *  1 => string vcTitle
         *  2 => string strSpaces
         */
        $arrOpts = array();
        foreach ($arrGroups as $key => $values) {
            /* Set an array with selected productgroup ID's */
            $arrSelected = array_column($product->arrValues["arrGroups"], "iCategoryID");
            /* Check if group ID is in arrSelected */
            $selected = in_array($values[0], $arrSelected) ? 1 : 0;
            /* Build option array with selected status */
            $arrOpts[] = array($values[0], $values[1], $values[2], $selected);
        }

        $product->arrValues["arrGroups"] = $arrOpts;

        $product->arrValues["iPrice"] = formatPrice($product->arrValues["iPrice"]);

        /* Call From Presenter */
        $form = new formPresenter($product->arrColumns, $product->arrValues);
        $form->formAction = "save";
        echo $form->presentform();

        sysBackendFooter();
        break;
    case "DELETE":
        $obj = new shopproduct();
        $id = filter_input(INPUT_GET, "id", FILTER_VALIDATE_INT);
        $obj->delete($id);
        header("location: ?mode=list");
        break;
    case "SAVE":
        $product = new shopproduct();
        $iPrice = (filter_input(INPUT_POST, "iPrice", FILTER_SANITIZE_NUMBER_FLOAT) / 100);
        $product->arrColumns["iPrice"]["Value"] = $iPrice;
        $iProductID = $product->save();
        header("Location: ?mode=details&iProductID=" . $iProductID);
        break;


}

