<?php
require_once filter_input(INPUT_SERVER, "DOCUMENT_ROOT") . "/cms/assets/incl/init.php";
$mode = setMode();
$strModuleName = "Brugere";


if ($auth->user->admin) {

    switch (strtoupper($mode)) {

        case "LIST":
            $strModuleMode = "Oversight";
            sysBackendHeader("Template");

            $arrButtonPanel = array();
            $arrButtonPanel[] = getButtonLink("plus", "?mode=edit&iUserID=-1", "Opret Ny Bruger", "");
            echo textPresenter::presentMode($strModuleName, $strModuleMode, $arrButtonPanel);

            $user = new user();
            $rows = $user->getAll();
            foreach ($rows as $key => $row) {
                $rows[$key]["opts"] = getIcon("?mode=edit&iUserID=" . $row["iUserID"], "rocket", "Redigere") .
                    getIcon("?mode=details&iUserID=" . $row["iUserID"], "globe", "Detajiler") .
                    getIcon("", "trash", "Slet element", "remove(" . $row["iUserID"] . ")");
            }
            $p = new listPresenter($user->arrLabels, $rows);
            echo $p->presentlist();

            sysBackendfooter("Template");
            break;

        case "DETAILS":
            $iUserID = Filter_input(INPUT_GET, "iUserID", FILTER_SANITIZE_NUMBER_INT);
            $user = new user();

            sysBackendHeader("Template");

            $strModuleMode = "Detaljer";
            $arrButtonPanel = array();
            $arrButtonPanel[] = getButtonLink("plus", "?mode=list", "Oversigt", "btn");
            $arrButtonPanel[] = getButtonLink("pencil", "?mode=setusergroups&iUserID=" . $iUserID, "Vælg grupper", "btn-success");
            $arrButtonPanel[] = getButtonLink("plus", "?mode=edit&iUserID=" . $iUserID, "Rediger", "btn");
            echo textPresenter::presentMode($strModuleName, $strModuleMode, $arrButtonPanel);
            $user = new user();
            $user->getItem($iUserID);
            $user->arrValues["daCreated"] = date("d. M Y", $user->arrValues["daCreated"]);
            $p = new listPresenter($user->arrColumns, $user->arrValues);
            echo $p->presentdetails();

            sysBackendfooter("Template");

            break;
        case "EDIT":
            $iUserID = Filter_input(INPUT_GET, "iUserID", FILTER_SANITIZE_NUMBER_INT);
            $strModuleMode = ($iUserID > 0) ? "Rediger" : "Opret ny useranisation";
            sysBackendHeader("Template");

            $arrButtonPanel = array();
            $arrButtonPanel[] = getButtonLink("plus", "?mode=list", "Oversigt", "btn");
            echo textPresenter::presentMode($strModuleName, $strModuleMode, $arrButtonPanel);
            $user = new user();
            if ($iUserID === -1) {
                unset($user->arrColumns["iSuspended"]);
            }
            if ($iUserID > 0) {
                $user->getItem($iUserID);
                unset($user->arrColumns["vcPassword"]);
                unset($user->arrColumns["vcPassword2"]);
            }
            $org = new org();
            $form = new formpresenter($user->arrColumns, $user->arrValues);
            echo $form->presentform();
            sysBackendfooter("Template");

            break;
        case "DELETE":
            $obj = new user();
            $id = filter_input(INPUT_GET, "id", FILTER_VALIDATE_INT);
            $obj->delete($id);
            header("location: ?mode=list");
            break;
        case "SAVE":
            $obj = new user();
            echo $iUserID = $obj->save();
            header("location: ?mode=details&iUserID=" . $iUserID);
            break;


        case "SETUSERGROUPS":
            $iUserID = filter_input(INPUT_GET, "iUserID", FILTER_SANITIZE_NUMBER_INT);

            $strModuleMode = "Bruger grupper";
            sysBackendHeader("Template");

            $arrButtonPanel = array();
            $arrButtonPanel[] = getButtonLink("eye", "?mode=details&iUserID=" . $iUserID, "Se gruppe", "btn-primary");
            $arrButtonPanel[] = getButtonLink("table", "?mode=list", "Oversigt", "btn-primary");
            echo textpresenter::presentMode($strModuleName, $strModuleMode, $arrButtonPanel);

            // Create array with user related groups
            $arrSelected = array();
            $params = array($iUserID);
            $strSelect = "SELECT iGroupID FROM usergrouprel WHERE iUserID = ?";
            $rel = $db->_fetch_array($strSelect, $params);
            foreach ($rel as $value) {
                $arrSelected[] = $value["iGroupID"];
            }

            // Get all groups from group object
            $group = new usergroup();
            $rows = $group->getAll();

            // Define arrColumns with user id field
            $arrColumns = array();
            $arrColumns["iUserID"] = array(
                "Formtype" => crud::INPUT_HIDDEN,
                "Required" => 0
            );

            // Define form values with user id value
            $arrFormValues = array();
            $arrFormValues["iUserID"] = $iUserID;

            // Loop rows and define arrColumns with checkboxes for group ids
            foreach ($rows as $key => $arrValues) {
                $field = "groups[" . $arrValues["iGroupID"] . "]";
                $arrColumns[$field] = array(
                    "Formtype" => crud::INPUT_CHECKBOX,
                    "Required" => 0,
                    "Label" => $arrValues["vcGroupName"]
                );
                // Set form values with related group ids
                $arrFormValues[$field] = in_array($arrValues["iGroupID"], $arrSelected) ? 1 : 0;
            }

            $form = new formpresenter($arrColumns, $arrFormValues);
            $form->formAction = "saveusergroups";
            echo $form->presentform();

            sysBackendfooter("Template");
            break;

        case "SAVEUSERGROUPS":
            $iUserID = filter_input(INPUT_POST, "iUserID", FILTER_SANITIZE_NUMBER_INT);

            // Delete existing user related groups
            $params = array($iUserID);
            $strDelete = "DELETE FROM usergrouprel WHERE iUserID = ?";
            $db->_query($strDelete, $params);

            // Create array for post filtering
            $args = array(
                "groups" => array(
                    "filter" => FILTER_VALIDATE_INT,
                    "flags" => FILTER_REQUIRE_ARRAY
                )
            );
            $arrInputVal = filter_input_array(INPUT_POST, $args);


            // Save user related groups if any
            if (count($arrInputVal["groups"])) {
                $arrGroups = array_keys($arrInputVal["groups"]);

                foreach ($arrGroups as $value) {
                    $params = array($iUserID, $value);
                    $strInsert = "INSERT INTO usergrouprel(iUserID, iGroupID) VALUES(?,?)";
                    $db->_query($strInsert, $params);
                }
            }
            header("Location: ?mode=details&iUserID=" . $iUserID);

            break;

    }
} else {
    echo $auth->loginform();
}

