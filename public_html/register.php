<?php
require_once filter_input(INPUT_SERVER, "DOCUMENT_ROOT") . "/cms/assets/incl/init.php";


$mode = setMode();
if (!$auth->user->extranet) {


    switch (strtoupper($mode)) {

        case "REGISTER";
            require_once filter_input(INPUT_SERVER, "DOCUMENT_ROOT") . "/assets/incl/nav.php";
            sysHeader("Template");
            $iUserID = filter_input(INPUT_GET, "iUserID", FILTER_SANITIZE_NUMBER_INT);

            $strHeader = ($iUserID > 0) ? "Rediger din profil" : "Opret profil";
            $strSubHeader = ($iUserID > 0) ? "Herunder kan du redigere din profil" : "Herunder kan du oprette en profil";

            $accHtml = "\n<section id=\"sign-up\">\n";
            $accHtml .= "<div class=\"container\">\n";
            $accHtml .= "<div class=\"row whitespace-sm\">\n";
            $accHtml .= "<h1 class=\"text-center\">$strHeader</h1>";
            $accHtml .= "<hr class=\"style1\">";
            $accHtml .= "<h3 class=\"text-center whitespace\">$strSubHeader</h3>";

            echo $accHtml;

            $user = new user();
            if ($iUserID > 0) {
                $user->getItem($iUserID);
            }
            ?>

            <?php

            $form = new formpresenter($user->arrColumns, $user->arrValues);
            $form->iUseEnctype = TRUE;

            echo $form->presentform();


            $accHtml = "</div>\n";
            $accHtml .= "</div>\n";
            $accHtml .= "</div>\n";
            echo $accHtml;

            sysFooter("Template");
            break;

        case "SAVE":
            $obj = new user();
            $iUserID = $obj->save();
            header("Location: index.php");
            break;


    }
} else {
    header("Location: index.php");
}