<?php
/*
 * Author @ Mads Roloff - Roloff-design
 */

//paths
$path = "Webshop";


//uno euro server!
//define("DOCROOT", "C:/Program Files (x86)/EasyPHP-Devserver-16.1/eds-www/VandreStoevlen/public_html");

//EasyPHP
define("DOCROOT", filter_input(INPUT_SERVER, "DOCUMENT_ROOT", FILTER_SANITIZE_STRING));
define("COREPATH", substr(DOCROOT, 0, strrpos(DOCROOT, "/")) . "/core/");

//Functions
require_once(COREPATH . 'functions.php');
require_once(COREPATH . 'constants.php');
/*ClassLoader*/
require_once COREPATH . "/classes/ClassLoader.php";
$Classloader = new ClassLoader();
//$db = new db();
$db = new dbconf();

$auth = new Auth();
$auth->iShowLoginForm = 0;
$auth->authentificate();

$cart = new shopcart();
$cart->getItemByUser($auth->iUserID);