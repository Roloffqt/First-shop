<?php
/**
 * Created by PhpStorm.
 * Auther @ Mads Roloff - Rights reservede to Author
 * Date: 07-02-2017
 */ ?>

<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/index.php"><i class="fa fa-home"></i> Home</a>
        </div>
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li>
                    <a href="/index.php"><i class="fa fa-dollar"></i> Shop</a>
                </li>
                <li>
                    <a href="#"><i class="fa fa-envelope"></i> Contact</a>
                </li>

            </ul>

            <ul class="nav navbar-nav navbar-right">
                <li>
                    <a href="assets/content/cart.php"><i class="fa fa-shopping-cart"></i> Kurv (<span id="productsInCart"><?php echo $cart->getCartQuantity(); ?></span>)</a>
                </li>
                <?php if ($auth->user->extranet) { ?>
                    <li>
                        <a href="?action=logout" class=" logout-btn btn btn-danger"> Log Ud</a>
                    </li>
                <?php } ?>
                <?php if (!$auth->user->extranet) { ?>
                    <li>
                        <a href="assets/content/login.php" class=" logout-btn btn btn-success"> Log ind!</a>
                    </li>
                <?php } ?>
            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </div>
    <!-- /.container -->
</nav>