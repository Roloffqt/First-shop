<?php
/**
 * Created by PhpStorm.
 * Auther @ Mads Roloff - Rights reservede to Author
 * Date: 07-02-2017
 */ ?>

<?php
$PageTitle = "Webshop";
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title><?php echo $PageTitle ?></title>
    <meta name="viewport" content="width=device-width" initial-scale="1"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <!-- CDNS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <!-- Custom css -->
    <link href="../../assets/css/Main.css" rel="stylesheet">
</head>
<body>

