<?php
/**
 * Created by PhpStorm.
 * Auther @ Mads Roloff - Rights reservede to Author
 * Date: 03-05-2017
 */


/**
 * Created by PhpStorm.
 * Auther @ Mads Roloff - Rights reservede to Author
 * Date: 01-05-2017
 */ ?>
<?php
require_once filter_input(INPUT_SERVER, "DOCUMENT_ROOT") . "/assets/incl/init.php";
$cartline = (int)filter_input(INPUT_POST, "cartlineid", FILTER_SANITIZE_NUMBER_INT, array("options" => array("min_range" => 1)));
$iQuantity = (int)filter_input(INPUT_POST, "quantity", FILTER_SANITIZE_NUMBER_INT, array("options" => array("min_range" => 1)));
$grandtotal = (int)filter_input(INPUT_POST, "cartTotal", FILTER_SANITIZE_NUMBER_INT, array("options" => array("min_range" => 1)));

if ($cartline && $iQuantity) {
    $cart->UpdateQuantity($iQuantity, $cartline);

    $arrJson = array(
        "status" => 1,
        "cartlineid" => $cartline,
        "cartTotal" => $cart->getGrandTotal(),
        "productsInCart" => $cart->getCartQuantity(),
        "priceFormat" => ",-"
    );
    echo json_encode($arrJson);
} else {
    $arrJson = array(
        "status" => 0
    );
    echo json_encode($arrJson);
}
?>


