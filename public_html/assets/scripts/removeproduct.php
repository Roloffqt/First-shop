<?php
/**
 * Created by PhpStorm.
 * Auther @ Mads Roloff - Rights reservede to Author
 * Date: 01-05-2017
 */ ?>
<?php
require_once filter_input(INPUT_SERVER, "DOCUMENT_ROOT") . "/assets/incl/init.php";
$iProductID = (int)filter_input(INPUT_POST, "iProductID", FILTER_SANITIZE_NUMBER_INT);

if (!isset($cart->iCartID)) {
    $cart->iCartID = $cart->create($auth->iUserID);
}

$cart->removeProduct($iProductID);

$arrJson = array(
    "productId" => $iProductID,
);
echo json_encode($arrJson);
?>

