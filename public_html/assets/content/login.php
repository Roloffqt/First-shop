<?php
/**
 * Created by PhpStorm.
 * Auther @ Mads Roloff - Rights reservede to Author
 * Date: 03-04-2017
 */ ?>
<?php
require_once filter_input(INPUT_SERVER, "DOCUMENT_ROOT") . "/assets/incl/init.php";
sysHeader();
require_once filter_input(INPUT_SERVER, "DOCUMENT_ROOT") . "/assets/incl/nav.php";

if (!$auth->user->extranet) {
    ?>
    <div class="container">

        <div class="col-lg-4">
            <h2>Login</h2>
            <form id="loginform" method="post" autocomplete="off">

                <div class="form-group">
                    <label for="username" class=""> Username</label>
                    <input type="text" class="form-control" id="username" autocapitalize="none" name="username" placeholder="Indtast brugernavn" value="">
                </div>

                <div class="form-group">
                    <label for="password" class=""> Password</label>
                    <input type="password" class="form-control" id="password" name="password" placeholder="password" value="">
                </div>

                <div class="btn-group">
                    <button type="submit" value="login" class="login-button"><i class="fa fa-chevron-right"></i></button>
                </div>

            </form>
        </div>


        <!--REGISTRATION FORM-->
        <div class="col-lg-push-4 col-lg-4">
            <h2>Register</h2>
            <!--Main Form-->
            <div class="login-form-1">
                <form id="" class="" onfocus="">
                    <div class="form-group">
                        <label for="username" class="required">Username</label>
                        <input type="text" class="form-control" id="username" name="username" data-requried="1" placeholder="Username" data-validate="username">
                    </div>
                    <div class="form-group">
                        <label for="password" class="required">Password</label>
                        <input type="password" class="form-control" id="password" name="password" placeholder="Password" data-requried="1" data-validate="password">
                    </div>
                    <div class="form-group">
                        <label for="password_confirm" class="required"> Password Confirm </label>
                        <input type="password" class="form-control" id="password_confirm" name="password_confirm" placeholder="Confirm Password" data-requried="1" data-validate="passwordConfirm">
                    </div>


                    <div class="form-group">
                        <label for="fullname" class="required"> Full Name </label>
                        <input type="text" class="form-control" id="fullname" name="fullname" placeholder="Full Name" data-validate="fullname">
                    </div>

                    <div class="form-group">
                        <label for="tel" class="required"> Telefon </label>
                        <input type="text" class="form-control" id="tel" name="tel" placeholder="Telefon" data-requried="1" data-validate="Telefon">
                    </div>

                    <div class="form-group">
                        <input type="radio" class="" name="gender" id="male">
                        <label for="Male">Male</label>

                        <input type="radio" class="" name="gender" id="female">
                        <label for="Female">Female</label>
                    </div>

                    <div class="form-group">
                        <input type="checkbox" class="" id="agree" name="agree" data-requried="1">
                        <label for="agree"> I agree with <a href="#">Terms of Service</a></label>
                    </div>

                    <button type="button" onclick="validate(this.form)" class="btn btn-success"><i class="fa fa-check"></i></button>


                </form>
            </div>
            <!--end:Main Form-->
        </div>
    </div>
    <?php
    sysFooter();
} else {
    header("Location: /index.php");
} ?>
