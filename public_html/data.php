<?php
/**
 * Created by PhpStorm.
 * Auther @ Mads Roloff - Rights reservede to Author
 * Date: 28-04-2017
 */ ?>


<?php
require_once filter_input(INPUT_SERVER, "DOCUMENT_ROOT") . "/assets/incl/init.php";


$ProductnewCount = $_POST['ProductnewCount'];
$product = new shopproduct();
$prod = $product->getAllLimit($ProductnewCount);
if ($prod > null) {
    foreach ($prod as $key => $row): ?>
        <a href="<?php echo "index.php?mode=Details&iProductID=" . $row["iProductID"] ?>">

            <div class="col-sm-4 col-lg-4 col-md-4 id="<?php echo "product" . $row["iProductID"] ?>">
            <div class="thumbnail">
                <img src="http://placehold.it/320x150" alt="">
                <div class="caption">
                    <h4 class="pull-right"><?php echo $row["iPrice"] ?>,-</h4>
                    <h4>
                        <a href="<?php echo "index.php?mode=Details&iProductID=" . $row["iProductID"] ?>"><?php echo $row["vcTitle"] ?></a>
                    </h4>
                    <p><?php echo substr($row["txDescShort"], 0, 35); ?></p>


                    <!--  <div class="col-md-12">


                                            <a href="<?php // echo $product->iProductID ?>" class="btn btn-info addtocart pull-right"
                                               data-productid=""> Add to
                                                cart</a>
                                            <input style="width: 50%;" class="form-control quantity pull-right" value="1" type="number" name="quantity">


                                            <div class="isincart"></div>
                                        </div>
                                            -->

                </div>
                <div class="ratings">
                    <p class="pull-right">15 reviews</p>
                    <p>
                        <span class="glyphicon glyphicon-star"></span>
                        <span class="glyphicon glyphicon-star"></span>
                        <span class="glyphicon glyphicon-star"></span>
                        <span class="glyphicon glyphicon-star"></span>
                        <span class="glyphicon glyphicon-star"></span>
                    </p>
                </div>
            </div>
            </div>
        </a>

    <?php endforeach;
} else {
    echo "No Products!";
}
?>