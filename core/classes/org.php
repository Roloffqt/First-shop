<?php

/**
 * Created by PhpStorm.
 * Auther @ Mads Roloff - Rights reservede to Author
 * Date: 30-01-2017
 */
class org extends crud
{

    protected $dbTable = "org";
    public $arrColumns = array();
    public $arrLabels = array();


    public function __construct()
    {
        parent::__construct($this->dbTable);
        //$this->arrColumns["daStart"]["Formtype"] = parent::INPUT_DATETIME;
        //$this->arrColumns["daStop"]["Formtype"] = parent::INPUT_DATETIME;
    }

    /**
     * @param $iItemID
     * get a single record
     */

    public function getItem($iItemID)
    {
        $this->arrValues = parent::getItem($iItemID);
        foreach ($this->arrValues as $key => $value) {
            $this->$key = $value;
        }
    }

    public function getAll()
    {
        $this->arrLabels = array(
            "opts" => "Options",
            "vcOrgName" => $this->arrColumns["vcOrgName"]["Label"],
            "daCreated" => $this->arrColumns["daCreated"]["Label"],
            "vcCountry" => $this->arrColumns["vcCountry"]["Label"]
        );
        $strSelect = "SELECT * FROM " . $this->dbTable . " WHERE iDeleted = 0";
        return $this->db->_fetch_array($strSelect);
    }

    /**
     *
     * @return Object
     */
    public function save()
    {
        return parent::saveItem();
    }

    public function delete($iItemID)
    {
        parent::delete($iItemID);
    }
}
