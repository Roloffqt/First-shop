<?php

/**
 * Description of List Presenter
 *
 * A class for element listing
 * Requires an array of fieldnames (Captions) for
 * a horizontal column list with a key matching array
 * of data for a vertical row list.
 * Optional sorting settings
 *
 * @author Heinz K, Nov 2016
 *
 * @param array arrLabels Array with db fieldname as key and friendly name as value
 * @param array arrValues Multiple array with db fieldname as key with matching db values
 * @param array arrOrderOpts Array with field names for list ordering options
 * @param bool hasSortOnSave A boolean flag to enable sort number savings on list
 * @param string strListClass Option to inject a custom class to the list table
 * @param string accHtml Accumulated html with list output
 *
 */
class listPresenter
{
    public $arrLabels;
    public $arrValues;

    public $arrOrderOpts;
    public $hasSortOnSave;
    public $strListClass;
    public $accHtml;

    public function __construct($arrLabels, $arrValues)
    {
        $this->arrLabels = $arrLabels;
        $this->arrValues = $arrValues;
        $this->arrOrderOpts = array();
        $this->hasSortOnSave = 0;
        $this->strListClass = "";
        $this->accHtml = "";
    }

    /**
     * Wraps array of fields and values in a html table
     * @return string Returns html list table
     */
    public function presentlist()
    {
        $this->accHtml = "<div>\n"
            . "<table class=\" table table-striped table-bordered table-hover table-responsive" . $this->strListClass . " \">\n"
            . "   <tr>\n";

        /* Loop column array and set table headers */
        foreach ($this->arrLabels as $value) {
            if (empty($value)) continue;
            $this->accHtml .= "<th><span class=\"pull-left\">" . $value . ":</span></th>\n";
        }
        $this->accHtml .= "</tr>\n";
        /* >> End table headers */

        /* Loop data row array */
        foreach ($this->arrValues as $row) {
            $this->accHtml .= "<tr>\n";
            /* Loop column array and get the matching key value from data rows */
            foreach ($this->arrLabels as $key => $value) {
                $value = (getAbbr($key) === "da") ? date("d M Y H:i", $row[$key]) : $row[$key];
                $nowrap = ($key === "opts") ? "nowrap" : "";
                $this->accHtml .= "<td " . $nowrap . ">" . $value . "</td>\n";
            }
            $this->accHtml .= "</tr>\n";
        }
        /* >> End datarows */

        $this->accHtml .= "</table>\n</div>\n";
        return $this->accHtml;
    }

    /**
     * Detail presenter list all information on a single element
     * Typically used in READ or DETAILS mode
     * @return string Returns accumulated html with details list
     */
    public function presentdetails()
    {
        $this->accHtml = "<div class=\"\">\n"
            . "<table class=\"striped bordered highlight responsive-table " . $this->strListClass . " \">\n";
        foreach ($this->arrValues as $key => $value) {
            if (isset($this->arrLabels[$key]) && $this->arrLabels[$key]["Label"]) {
                $this->accHtml .= "<tr>\n";
                $this->accHtml .= "   <td class='col-sm-2'><b>" . $this->arrLabels[$key]["Label"] . ":</b></td>\n";
                $this->accHtml .= "   <td class='col-sm-10'>" . $value . "   </td>\n";
                $this->accHtml .= "</tr>\n";
            }
        }

        $this->accHtml .= "</table>\n";
        return $this->accHtml;
    }
}
