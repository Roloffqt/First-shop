<?php

/**
 * Created by PhpStorm.
 * Auther @ Mads Roloff - Rights reservede to Author
 * Date: 30-01-2017
 */
class usergroup extends crud
{

    protected $dbTable = "usergroup";
    public $arrColumns = array();
    public $arrLabels = array();


    public function __construct()
    {
        parent::__construct($this->dbTable);
    }

    /**
     * @param $iItemID
     * get a single record
     */

    public function getItem($iItemID)
    {
        $this->arrValues = parent::getItem($iItemID);
        foreach ($this->arrValues as $key => $value) {
            $this->$key = $value;
        }
    }

    public function getAll()
    {
        $this->arrLabels = array(
            "opts" => "Options",
            "vcGroupName" => $this->arrColumns["vcGroupName"]["Label"],
            "daCreated" => $this->arrColumns["daCreated"]["Label"],
            "txDesc" => $this->arrColumns["txDesc"]["Label"]
        );
        $strSelect = "SELECT * FROM " . $this->dbTable . " WHERE iDeleted = 0";
        return $this->db->_fetch_array($strSelect);
    }

    /**
     *
     * @return Object
     */
    public function save()
    {
        return parent::saveItem();
    }

    public function delete($iItemID)
    {
        parent::delete($iItemID);
    }
}
