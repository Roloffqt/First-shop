<?php

/**
 * Created by PhpStorm.
 * Auther @ Mads Roloff - Rights reservede to Author
 * Date: 30-01-2017
 */
class shopproduct extends crud
{

    protected $dbTable = "shopproduct";
    public $arrColumns = array();
    public $arrLabels = array();
    public $arrValues = array();

    public function __construct()
    {
        parent::__construct($this->dbTable);
        $this->arrColumns["txDesc"]["Formtype"] = parent::INPUT_TEXTEDITOR;
        $this->arrColumns["txDescShort"]["Formtype"] = parent::INPUT_TEXTEDITOR;
        $this->arrColumns["iPrice"]["Formtype"] = parent::INPUT_TEXT;
        $this->arrColumns["iSortNum"]["Formtype"] = parent::INPUT_TEXT;
        $this->arrColumns["iStock"]["Formtype"] = parent::INPUT_TEXT;

    }

    /**
     * @param $iItemID
     * get a single record
     */
    public function getItem($iItemID)
    {
        $this->arrValues = parent::getItem($iItemID);

        $this->arrValues["arrGroups"] = $this->getgrouprelations();

        foreach ($this->arrValues as $key => $value) {
            $this->$key = $value;
        }


    }

    public function listOptions()
    {
        $strSelect = "SELECT iCategoryID, vcTitle " .
            "FROM " . $this->dbTable . " " .
            "WHERE iParentID = -1 " .
            "AND iDeleted = 0";
        return $this->db->_fetch_array($strSelect);
    }

    public function getAll()
    {
        $this->arrLabels = array(
            "opts" => "Options",
            "vcTitle" => $this->arrColumns["vcTitle"]["Label"],
            "daCreated" => $this->arrColumns["daCreated"]["Label"],
            "txDescShort" => $this->arrColumns["txDescShort"]["Label"],
            "iIsActive" => $this->arrColumns["iIsActive"]["Label"]
        );
        $strSelect = "SELECT * FROM " . $this->dbTable . " WHERE iDeleted = 0";
        return $this->db->_fetch_array($strSelect);
    }

    public function getAllLimit($limit = 2)
    {
        $this->arrLabels = array(
            "opts" => "Options",
            "vcTitle" => $this->arrColumns["vcTitle"]["Label"],
            "daCreated" => $this->arrColumns["daCreated"]["Label"],
            "txDescShort" => $this->arrColumns["txDescShort"]["Label"],
            "iIsActive" => $this->arrColumns["iIsActive"]["Label"]
        );
        $strSelect = "SELECT * FROM " . $this->dbTable . " WHERE iDeleted = 0 LIMIT $limit";
        return $this->db->_fetch_array($strSelect);
    }

    public function getStuff($limit = 0)
    {
        $strSelect = "SELECT * FROM shopproduct LIMIT $limit";
        return $this->db->_fetch_array($strSelect);
    }

    public function getgrouprelations()
    {
        $params = array($this->arrValues["iProductID"]);
        $strSelect = "SELECT c.iCategoryID, c.vcTitle 
                      FROM shopcategory c 
                      JOIN shopcatprodrel x 
                      ON x.iCategoryID = c.iCategoryID 
                      WHERE x.iProductID = ? AND c.iDeleted = 0";
        return $this->db->_fetch_array($strSelect, $params);
    }


    /**
     *
     * @return Object
     */
    /**
     * Save item
     */
    public function save()
    {
        $iProductID = parent::saveItem();

        /* Remove all group relations for the product */
        $params = array($iProductID);
        $strDelete = "DELETE FROM shopcatprodrel WHERE iProductID = ?";
        $this->db->_query($strDelete, $params);

        /* Create arguments for post filtering */
        $args = array(
            "arrGroups" => array(
                "filter" => FILTER_VALIDATE_INT,
                "flags" => FILTER_REQUIRE_ARRAY
            )
        );
        $arrInputVal = filter_input_array(INPUT_POST, $args);

        /* Save user related groups if any */
        if (count($arrInputVal["arrGroups"])) {
            $arrValues = array_values($arrInputVal["arrGroups"]);
            foreach ($arrValues as $value) {
                $params = array($iProductID, $value);
                $strInsert = "INSERT INTO shopcatprodrel(iProductID, iCategoryID) VALUES(?,?)";
                $this->db->_query($strInsert, $params);
            }
        }
        return $iProductID;
    }

    public function delete($iItemID)
    {
        parent::delete($iItemID);
    }
}
