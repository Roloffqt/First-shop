<?php

class shopcart extends crud
{
    protected $dbTable = "shopcart";
    public $arrColumns = array();
    public $arrLabels = array();
    public $arrValues = array();
    public $module = crud::MOD_WEBSHOP;

    public function __construct()
    {
        parent::__construct($this->dbTable);
    }


    public function getItemByUser($iUserID)
    {
        $strSelectCart = "SELECT iCartID FROM shopcart " .
            " WHERE ";
        if ($iUserID) {

            $strSelectSession = "SELECT iCartID FROM shopcart " .
                "WHERE vcSessionID = ? " .
                "AND iUserID = 0";

            if ($this->db->_fetch_value($strSelectSession, array(session_id()))) {
                $strUpdateSession = "UPDATE " . $this->dbTable . " SET " .
                    "iUserID = ? " .
                    "WHERE vcSessionID = ?";

                $this->db->_query($strUpdateSession, array($iUserID, session_id()));
            }

            $strSelectCart .= " iUserID = ?";
            $params = array($iUserID);

        } else {
            $strSelectCart .= " vcSessionID = ?";
            $params = array(session_id());
        }

        $iCartID = $this->db->_fetch_value($strSelectCart, $params);
        $this->getItem($iCartID);
        return $iCartID;
    }

    public function getCartLines($iItemID)
    {
        $params = array($iItemID);
        $strSelect = "SELECT * FROM shopcartline WHERE iCartID = ?";
        return $this->db->_fetch_array($strSelect, $params);
    }

    public function create($iUserID = 0)
    {
        $sql = "INSERT INTO " . $this->dbTable . "(iUserID, vcSessionID, daCreated) VALUES(?,?,?)";
        $this->db->_query($sql, array($iUserID, session_id(), time()));
        return $this->db->_getinsertid();
    }

    public function removeProduct($iProductID)
    {
        $sql = "DELETE FROM shopcartline WHERE iCartID = ? AND iProductID = ?";
        $this->db->_query($sql, array($this->iCartID, $iProductID));
    }


    public function UpdateQuantity($Quantity, $iCartlineID)
    {
        $sql = "UPDATE shopcartline SET iQuantity = ? WHERE iCartID = ? AND iCartlineID = ?";
        $this->db->_query($sql, array($Quantity, $this->iCartID, $iCartlineID));
    }


    public function addProduct($iProductID, $iQuantity)
    {
        $strInsert = "INSERT INTO shopcartline(iCartID, iProductID, iQuantity) VALUES(?,?,?)";
        $this->db->_query($strInsert, array($this->iCartID, $iProductID, $iQuantity));
    }

    public function getCartQuantity()
    {
        $num = 0;
        if (isset($this->iCartID) && $this->iCartID > 0) {
            $strSelectCart = "SELECT SUM(iQuantity) FROM shopcartline WHERE iCartID = ?";
            return $this->db->_fetch_value($strSelectCart, array($this->iCartID));
        }
        return $num;
    }

    public function getGrandTotal()
    {
        if (isset($this->iCartID) && $this->iCartID > 0) {
            $strSelectCart = "SELECT SUM(oi.iQuantity * p.iPrice) AS grand_total FROM shopcartline oi JOIN shopproduct p ON p.iProductID = oi.iProductID WHERE oi.iCartID = " . $this->iCartID;
            return $this->db->_fetch_value($strSelectCart, ($this->iCartID));
        }
    }

    public function getStuff($limit = 0)
    {
        $strSelect = "SELECT * FROM shopproduct LIMIT $limit";
        return $this->db->_fetch_array($strSelect);
    }


    /*
     * Get a single record
     * param int $iItemID
     */
    public function getItem($iItemID)
    {
        $this->arrValues = parent::getItem($iItemID);
        $this->arrValues["arrCartLines"] = $this->getCartLines($iItemID);
        foreach ($this->arrValues as $key => $value) {
            $this->$key = $value;
        }
    }
}