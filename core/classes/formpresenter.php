<?php

/**
 * Created by PhpStorm.
 * Author @ Mads Roloff - Tech College Aalborg
 * Date: 07-12-2016
 */
class formpresenter
{

    public $arrColumns;
    public $arrValues;
    public $arrButtons;
    public $aH;
    public $formId;
    public $formMethod;
    public $formAction;
    public $formClass;
    public $iUseEnctype;
    public $strFormId;


    public function __construct($arrColumns, $arrValues)
    {
        $this->arrColumns = $arrColumns;
        $this->arrValues = $arrValues;
        $this->arrButtons = array();
        $this->formId = "adminform";
        $this->formMethod = "POST";
        $this->formAction = "save";
        $this->formClass = "";
        $this->aH = "";
    }

    public function presentform()
    {
        $strEnctype = ($this->iUseEnctype === TRUE) ? "enctype='multipart/form-data'" : "";

        $this->aH = "<form method='$this->formMethod' Id='$this->formId'  class='form-horizontal $this->formClass' " . $strEnctype . ">\n"
            . " <fieldset class='fsStyle'>\n"
            . "  <input type='hidden' name='mode' value='" . $this->formAction . "'>\n";
        //Loops throught arrColumns and makes it keys be $values

        foreach ($this->arrColumns as $key => $values) {
            $strIsRequired = (isset($values["Required"]) && $values["Required"] > 0) ? "required" : "";

            if (!empty($strIsRequired)) {
                $arrIsRequired[$key] = array($values["Label"], $values["Formtype"]);
            }

            if (isset($values["Formtype"])) {
                switch ($values["Formtype"]) {
                    case crud::INPUT_HIDDEN:
                        //Makes a hidden input field with the ID
                        $this->aH .= "<input type='hidden' name='" . $key . "' id='" . $key . "' value='" . $this->arrValues[$key] . "'>\n";
                        break;
                    case crud::INPUT_TEXT:
                    case crud::INPUT_TEXT_READONLY:
                    case crud::INPUT_EMAIL:
                    case crud::INPUT_PASSWORD:
                        $strReadOnly = ($values["Formtype"] === crud::INPUT_TEXT_READONLY) ? "readonly" : "";
                        //Makes visible input fields for anything i i make I_text in arrValues array, and then prints it with the key from arrColumns as names & ID
                        $this->aH .= "<div class='form-group'>";
                        $this->aH .= "<label class='. $strIsRequired .'> " . $values["Label"] . " </label>";
                        $this->aH .= "<input " . $strReadOnly . " type='" . $values['Formtype'] . "' class='form-control' name ='" . $key . "'id='" . $key . "'value='" . $this->arrValues[$key] . "'>  \n";
                        $this->aH .= "</div>";


                        break;
                    case crud::INPUT_TEXTAREA:
                        //Makes visible textarea fields for anything i i make I_text in arrValues array, and then prints it with the key from arrColumns as names & ID
                        $this->aH .= "<div class='form-group'>";
                        $this->aH .= "<label class='. $strIsRequired .'>" . $values["Label"] . "</label>";
                        $this->aH .= "<textarea type='text' class='form-control' name ='" . $key . "' id='" . $key . "'>" . $this->arrValues[$key] . "</textarea>\n";
                        $this->aH .= "</div>";
                        break;

                    case crud::INPUT_TEXTEDITOR:
                        $this->aH .= "<div class='form-group'>";
                        $this->aH .= "<label class='. $strIsRequired .'>" . $values["Label"] . "</label>";
                        $this->aH .= "<textarea type=\"text\" class=\"summernote\" name =\"" . $key . "\" id=\"" . $key . "\">" . $this->arrValues[$key] . "</textarea>\n";
                        $this->aH .= "</div>";
                        break;

                    case crud::INPUT_SELECT:
                        $this->aH .= "<div class='form-group'>";
                        $this->aH .= "<label class='. $strIsRequired .'> " . $values["Label"] . " </label>";
                        $this->aH .= $this->arrValues[$key];
                        $this->aH .= "</div>";
                        break;

                    case crud::INPUT_DATE:
                        $stamp = ($this->arrValues[$key] > 0) ? $this->arrValues[$key] : time();
                        $d = new dateselector($stamp);
                        $this->aH .= "<div class=\"row\">";
                        $this->aH .= "<div class=\"col-sm-offset-1\">";
                        $this->aH .= "<label class=\"control-label " . $strIsRequired . "\" for=\"" . $key . "\">" . $values["Label"] . ":</label>\n";
                        $this->aH .= "</div>\n";
                        $this->aH .= "</div>\n";
                        $this->aH .= "<div class=\"row\">";
                        $this->aH .= "   <div class=\"col-sm-offset-1 s2 \">\n";
                        $this->aH .= "   " . $d->dateselect("DAY", $key);
                        $this->aH .= "</div>\n";
                        $this->aH .= "   <div class=\"col-sm-2 \">\n";
                        $this->aH .= "   " . $d->dateselect("MONTH", $key);
                        $this->aH .= "</div>\n";
                        $this->aH .= "   <div class=\"col-sm-2 \">\n";
                        $this->aH .= "   " . $d->dateselect("YEAR", $key);
                        $this->aH .= "   </div>\n";
                        $this->aH .= "</div>\n";
                        break;
                    case crud::INPUT_DATETIME:
                        $stamp = ($this->arrValues[$key] > 0) ? $this->arrValues[$key] : time();
                        $d = new dateselector($stamp);
                        $this->aH .= "<div class=\" row\">";
                        $this->aH .= "<div class=\"col-sm-1\">";
                        $this->aH .= "<label class=\"control-label\"" . $strIsRequired . "\" for=\"" . $key . "\">" . $values["Label"] . ":</label>\n";
                        $this->aH .= "</div>\n";
                        $this->aH .= "</div>\n";
                        $this->aH .= "<div class=\"row\">";
                        $this->aH .= "   <div class=\"col-sm-2 \">\n";
                        $this->aH .= "   " . $d->dateselect("DAY", $key);
                        $this->aH .= "</div>\n";
                        $this->aH .= "   <div class=\"col-sm-2 \">\n";
                        $this->aH .= "   " . $d->dateselect("MONTH", $key);
                        $this->aH .= "</div>\n";
                        $this->aH .= "   <div class=\"col-sm-2 \">\n";
                        $this->aH .= "   " . $d->dateselect("YEAR", $key);
                        $this->aH .= "   </div>\n";
                        $this->aH .= "   <div class=\"col-sm-2 \">\n";
                        $this->aH .= "   " . $d->dateselect("HOURS", $key);
                        $this->aH .= "   </div>\n";
                        $this->aH .= "   <div class=\"col-sm-2 \">\n";
                        $this->aH .= "   " . $d->dateselect("MINUTES", $key);
                        $this->aH .= "   </div>\n";
                        $this->aH .= "</div>\n";
                        break;
                    case crud::INPUT_CHECKBOX:
                        $strIsChecked = $this->arrValues[$key] ? "checked=\"checked\"" : "";
                        $this->aH .= "<div class=\"row\">\n";
                        $this->aH .= "   <div class=\"col-sm-2 form-control\">\n";
                        $this->aH .= "       <label class='. $strIsRequired .' for='$key' >" . $values["Label"] . "  </label>\n";
                        $this->aH .= "   <input type=\"checkbox\" id=\"" . $key . "\"name=\"" . $key . "\" value=\"" . $key . "\" " . $strIsChecked . ">\n";
                        $this->aH .= "   </div>\n";
                        $this->aH .= "   </div>\n";
                        break;
                    case crud::INPUT_CHECKBOXMULTI:
                        $this->aH .= "<div class=\"form-group\">\n";
                        $this->aH .= "   <label class=\"col-sm-3 control-label\" for=\"" . $key . "\">" . $values["Label"] . ":</label>\n";
                        $this->aH .= "   <div class=\"col-sm-9\">\n";
                        $this->aH .= "      <div class=\"checkbox\">\n";

                        /**
                         * $optValues:
                         * 0 => int iGroupID
                         * 1 => string vcTitle
                         * 2 => string spaces
                         * 3 => bool Selected Status
                         */

                        foreach ($this->arrValues[$key] as $k => $optValues) {
                            $strTitle = empty($optValues[2]) ? "<b>" . $optValues[1] . "</b>" : $optValues[1];
                            $strChecked = ($optValues[3] > 0) ? "checked" : "";
                            $this->aH .= "         <label>\n";
                            $this->aH .= $optValues[2] . "<input type=\"checkbox\" " . $strChecked . " name=\"" . $key . "[]\" " .
                                "id=\"" . $optValues[0] . "\" value=\"" . $optValues[0] . "\"></input>" . $strTitle . "\n";
                            $this->aH .= "         </label><br />\n";
                        }
                        $this->aH .= "      </div>\n";
                        $this->aH .= "   </div>\n";
                        $this->aH .= "</div>\n";
                        break;

                }
            }
        }


        //  $val = new Validate();
        // $val->strFormId = $this->formId;
        // $val->arrFields = $arrIsRequired;
        //  $this->aH .= $val->setValidate();


        //Makes a buttom from my getButtom in Functions
        $this->aH .= "<div class=''>\n";
        $this->aH .= getButton("submit", "Gem");
        $this->aH .= getButton("button", "Annulere", "goback()") . "\t";
        $this->aH .= " </div > \n";
        $this->aH .= " </fieldset > \n</form > \n";

        return $this->aH;
    }
}








