<?php

class shopcategory extends crud
{
    protected $dbTable = "shopcategory";
    public $arrColumns = array();
    public $arrLabels = array();
    public $arrValues = array();

    public function __construct()
    {
        parent::__construct($this->dbTable);
        $this->arrColumns["txDesc"]["Formtype"] = parent::INPUT_TEXTEDITOR;
        $this->arrColumns["iSortNum"]["Formtype"] = parent::INPUT_TEXT;
    }

    public function listByParent($iParentID)
    {
        $this->arrLabels = array(
            "opts" => "Options",
            "vcTitle" => $this->arrColumns["vcTitle"]["Label"],
            "daCreated" => $this->arrColumns["daCreated"]["Label"],
            "iSortNum" => $this->arrColumns["iSortNum"]["Label"],
        );

        $params = array($iParentID);
        $strSelect = "SELECT * " .
            "FROM " . $this->dbTable . " " .
            "WHERE iParentID = ? " .
            "AND iDeleted = 0";
        return $this->db->_fetch_array($strSelect, $params);
    }

    public function getopts($iParentID, &$arrGroups = array(), $strPadding = "")
    {

        $params = array($iParentID);
        $strSelect = "SELECT iCategoryID, vcTitle FROM " . $this->dbTable . " " .
            "WHERE iParentID = ? " .
            "AND iDeleted = 0 " .
            "ORDER BY iSortNum";
        $row = $this->db->_fetch_array($strSelect, $params);
        foreach ($row as $key => $values) {
            $arrGroups[] = array($values["iCategoryID"], $values["vcTitle"], $strPadding);
            self::getopts($values["iCategoryID"], $arrGroups, $strPadding . "   &nbsp&nbsp&nbsp&nbsp");
        }
        return $arrGroups;
    }


    public function listOptions()
    {
        $strSelect = "SELECT iCategoryID, vcTitle " .
            "FROM " . $this->dbTable . " " .
            "WHERE iDeleted = 0";
        return $this->db->_fetch_array($strSelect);
    }

    /*
     * Get a single record
     * param int $iItemID
     */
    public function getItem($iItemID)
    {
        $this->arrValues = parent::getItem($iItemID);
        foreach ($this->arrValues as $key => $value) {
            $this->$key = $value;
        }
    }

    // Returns object save
    public function save()
    {
        return parent::saveItem();
    }

    public function delete($iItemID)
    {
        parent::delete($iItemID);
    }


}