<?php

/**
 * Created by PhpStorm.
 * Auther @ Mads Roloff - Rights reservede to Author
 * Date: 30-01-2017
 */
class user extends crud
{

    protected $dbTable = "user";
    public $arrColumns = array();
    public $arrLabels = array();
    public $arrValues = array();
    public $sysadmin;
    public $admin;
    public $extranet;
    public $newsletter;


    public function __construct()
    {
        parent::__construct($this->dbTable);
        unset($this->arrColumns["iUserRole"]);
        unset($this->arrColumns["iOrgID"]);
        $this->arrColumns["vcPassword"]["FormType"] = parent::INPUT_PASSWORD;
        $this->arrColumns["vcPassword2"]["FormType"] = parent::INPUT_PASSWORD;
        //$this->arrColumns["daStart"]["Formtype"] = parent::INPUT_DATETIME;
        //$this->arrColumns["daStop"]["Formtype"] = parent::INPUT_DATETIME;

    }

    /**
     * @param $iItemID
     * get a single record
     */

    public function getItem($iItemID)
    {
        $this->arrValues = parent::getItem($iItemID);

        if ($this->arrValues["iOrgID"] > 0) {
            $org = new org();
            $org->getItem($this->arrValues["iOrgID"]);
            $this->arrValues["org"] = $org->arrValues;
        }

        $this->arrValues["arrUserGroups"] = $this->getgrouprelations();

        foreach ($this->arrValues as $key => $value) {
            $this->$key = $value;
        }
        foreach ($this->arrValues["arrUserGroups"] as $value) {
            $role = strtolower($value["vcRoleName"]);
            $this->$role = 1;

        }
    }


    public function getgrouprelations()
    {
        $params = array($this->arrValues["iUserID"]);
        $strSelect = "SELECT g.iGroupID, g.vcGroupName, g.vcRoleName FROM usergroup g, usergrouprel x WHERE x.iUserID = ? AND x.iGroupID = g.iGroupID AND g.iDeleted = 0;";
        return $this->db->_fetch_array($strSelect, $params);
    }


    public function getAll()
    {
        $this->arrLabels = array(
            "opts" => "Options",
            "vcUserName" => $this->arrColumns["vcUserName"]["Label"],
            "daCreated" => $this->arrColumns["daCreated"]["Label"],
            "vcCity" => $this->arrColumns["vcCity"]["Label"]
        );
        $strSelect = "SELECT * FROM " . $this->dbTable . " WHERE iDeleted = 0";
        return $this->db->_fetch_array($strSelect);
    }

    public function getOpitions()
    {
        $this->arrLabels = array(
            "opts" => "Options",
        );
        $strSelect = "SELECT iUserID, CONCAT(vcFirstName, ' ', vcLastName) as vcName FROM user WHERE iDeleted = 0 ORDER BY vcLastName, vcFirstName";
        return $this->db->_fetch_array($strSelect);
    }


    /**
     * @return Object
     */
    public function save()
    {
        return parent::saveItem();
    }

    public function delete($iItemID)
    {
        parent::delete($iItemID);
    }


}
