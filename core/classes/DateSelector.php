<?php

/**
 * Created by PhpStorm.
 * Auther @ Mads Roloff - Rights reservede to Author
 * Date: 14-12-2016
 */
class DateSelector
{

    public $stamp;
    public $strName;
    public $strFormat;
    public $accHtml;
    public $useLocalNames;
    public $minInVal;

    private $arrTerms;

    public $arrDay2Local = array(1 => "Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday");

    public $arrMonth2Local = array(1 => "January", "February", "March", "April", "May", "June", "July", "August",
        "October", "September", "November", "December");


    public function __construct($stamp)
    {
        $this->stamp = $stamp;
        $this->strName = "";
        $this->strFormat = "";
        $this->useLocalNames = TRUE;
        $this->minInVal = "1";
        $this->aH = "";


    }

    public function dateSelect($strFormat, $strName)
    {
        $this->strFormat = $strFormat;
        $this->strName = $strName;
        $this->initTerms();

//     ********   HTML SELECTBOX HTML
        $this->aH = "<select class=\"form-control\" name= '" . $this->strName . "_" . $this->strFormat . "'>";
        $freq = ($this->strFormat === "minutes") ? $this->minInVal : 1;
        for ($i = $this->arrTerms["numFloor"]; $i < $this->arrTerms["numCeil"]; $i += $freq) {
            $strOptText = $this->getOptText($i);
            $strSelected = (str_pad($i, 2, 0, STR_PAD_LEFT) === $this->arrTerms["numSelected"]) ? "selected" : "";
            $this->aH .= "<option value=\"" . $i . "\" " . $strSelected . ">" . $strOptText . "</option>\n";
        }
        $this->aH .= "</select>\n";
        return $this->aH;
    }

    private function getOptText($val)
    {

        switch (strtoupper($this->strFormat)) {
            case "MONTH":
                if ($this->useLocalNames) {
                    $val = $this->arrMonth2Local[$val];
                }
                break;
            case "HOURS":
                $val = str_pad($val, 2, 0, STR_PAD_LEFT);
                break;
        }
        return $val;

    }

    private function initTerms()
    {
        switch (strtoupper($this->strFormat)) {
            case "DAY":
                $this->arrTerms = array(
                    "numSelected" => date("d", $this->stamp),
                    "numFloor" => 1,
                    "numCeil" => 32
                );
                break;
            case "MONTH":
                $this->arrTerms = array(
                    "numSelected" => date("m", $this->stamp),
                    "numFloor" => 1,
                    "numCeil" => 13,
                );
                break;
            case "YEAR":
                $this->arrTerms = array(
                    "numSelected" => date("Y", $this->stamp),
                    "numFloor" => date("Y", $this->stamp) - 100,
                    "numCeil" => date("Y", $this->stamp) + 60
                );
                break;
            case "HOURS":
                $this->arrTerms = array(
                    "numSelected" => date("H", $this->stamp),
                    "numFloor" => "00",
                    "numCeil" => 23
                );
                break;
            case "MINUTES":
                $this->arrTerms = array(
                    "numSelected" => date("i", $this->stamp),
                    "numFloor" => 00,
                    "numCeil" => 59
                );
                break;
        }


    }


}
/*class DateSelector
{

    public $stamp;
    public $sName;
    public $sFormat;
    public $aH;
    public $uLocalNames;
    public $MinVal;
    public $freg;
    private $arrTerms;


    public $arrDay2Local = array(1 => "Søndag", "Mandag", "Tirsdag",
        "Onsdag", "Torsdag", "Fredag", "Lørdag");
    // Friendly Danish Day Names for select box

    public $arrMonth2Local = array(1 => "Januar", "Februar", "Marts", "April", "Maj",
        "Juni", "Juli", "August", "September", "Oktober",
        "November", "December");

    // Friendly Danish Month Names for select box

    public function __construct($stamp)
    {
        $this->stamp = $stamp;
        //$stamp = Timestamp Value
        $this->sName = "";
        //sName Name for the select box
        $this->sFormat = "";
        //sFormat Format for the select box D/M/Y/H/m
        $this->uLocalNames = TRUE;
        //IF true use Local names arrays
        $this->MinVal = 1;
        //Interval between the minutes in the min Select box

        $this->aH = "";
    }

    public function DSelect($sFormat, $sName)
    {
        $this->sName = $sName;
        $this->sFormat = $sFormat;
        //Prepares for array
        $this->initTerms();
        //Where i make Selectbox HTML


        $this->aH = "<select name='" . $this->sName . "_" . $this->sFormat . "'>";
        $freg = $this->sFormat === "MIN" ? $this->MinVal : 1;
        for ($i = $this->arrTerms["numFloor"]; $i <= $this->arrTerms["numCeil"]; $i += $freg) {
            $sOptText = $this->getOptText($i);
            //Gets opition text
            $sSelected = (str_pad($i, 2, 0, STR_PAD_LEFT) === $this->arrTerms["numSelected"]) ? "Selected" : "";
            //Sets a "padding" that if the number is 1-9 it adds a zero in front
            $this->aH .= "<option value='" . $i . "' " . $sSelected . ">" . $sOptText . "</option>\n";
            //Adds option in the HTML
        }
        $this->aH .= "</select>";

        return $this->aH;
    }

    private function getOptText($v)
    {
        switch (strtoupper($this->sFormat)) {
            case "MONTH":
                if ($this->uLocalNames === TRUE) {
                    //IF uLocalNames = True then do V
                    $v = $this->arrMonth2Local[$v];
                    //Sets Val to be the same as arrMonth2Local's value
                }
                break;
            case "HOURS":
            case "MIN":
                $v = str_pad($v, 2, 0, STR_PAD_LEFT);
                break;
        }
        return $v;
    }

    private function initTerms()
    {
        switch (strtoupper($this->sFormat)) {
            case "DAY":
                $this->arrTerms = array(
                    "numSelected" => date('D', $this->stamp),
                    "numFloor" => 1,
                    "numCeil" => 31,
                );
                break;
            case "MONTH":
                $this->arrTerms = array(
                    "numSelected" => date("M", $this->stamp),
                    "numFloor" => 1,
                    "numCeil" => 12,
                );
                break;
            case "YEAR":
                $this->arrTerms = array(
                    "numSelected" => date("Y", $this->stamp),
                    "numFloor" => date("Y", $this->stamp) - 100,
                    "numCeil" => date("Y", $this->stamp) + 20,
                );
                break;
            case "HOUR":
                $this->arrTerms = array(
                    "numSelected" => date("i", $this->stamp),
                    "numFloor" => 1,
                    "numCeil" => 23,
                );
                break;
            case "MIN":
                $this->arrTerms = array(
                    "numSelected" => date("i", $this->stamp),
                    "numFloor" => "00",
                    "numCeil" => 59,
                );
                break;
        }
    }
}*/