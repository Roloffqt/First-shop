<?php

/**
 * Created by PhpStorm.
 * Auther @ Mads Roloff - Rights reservede to Author
 * Date: 30-01-2017
 */
class event extends crud
{

    protected $dbTable = "event";
    public $arrColumns = array();
    public $arrLabels = array();

    public function __construct()
    {
        parent::__construct($this->dbTable);
        $this->arrColumns["daStart"]["Formtype"] = parent::INPUT_DATETIME;
        $this->arrColumns["daStop"]["Formtype"] = parent::INPUT_DATETIME;
        $this->arrColumns["vcType"]["Formtype"] = parent::INPUT_SELECT;
        $this->arrColumns["vcTypeName"]["Formtype"] = parent::INPUT_SELECT;
    }

    /**
     * @param $iItemID
     * get a single record
     */
    public function getItem($iItemID)
    {
        $this->arrValues = parent::getItem($iItemID);
        foreach ($this->arrValues as $key => $value) {
            $this->$key = $value;
        }
    }

    public function getAll()
    {
        $this->arrLabels = array(
            "opts" => "Options",
            "vcTitle" => $this->arrColumns["vcTitle"]["Label"],
            "daCreated" => $this->arrColumns["daCreated"]["Label"],
            "txShortDesc" => $this->arrColumns["txShortDesc"]["Label"],
            "isActive" => $this->arrColumns["isActive"]["Label"]
        );
        $strSelect = "SELECT * FROM " . $this->dbTable . " WHERE iDeleted = 0";
        return $this->db->_fetch_array($strSelect);
    }

    public function getAllEvent($Limit)
    {
        $this->arrLabels = array(
            "opts" => "Options",
            "vcTitle" => $this->arrColumns["vcTitle"]["Label"],
            "vcImage" => $this->arrColumns["vcImage"]["Label"],
            "vcBtnText" => $this->arrColumns["vcBtnText"]["Label"],
            "daCreated" => $this->arrColumns["daCreated"]["Label"],
            "txShortDesc" => $this->arrColumns["txShortDesc"]["Label"],
            "isActive" => $this->arrColumns["isActive"]["Label"]
        );
        $strSelect = "SELECT * FROM " . $this->dbTable . " WHERE iDeleted = 0 ORDER BY iEventID DESC LIMIT " . $Limit;
        return $this->db->_fetch_array($strSelect);
    }

    public function getNewestEvent($Limit)
    {
        $this->arrLabels = array(
            "opts" => "Options",
            "vcTitle" => $this->arrColumns["vcTitle"]["Label"],
            "vcImage" => $this->arrColumns["vcImage"]["Label"],
            "vcBtnText" => $this->arrColumns["vcBtnText"]["Label"],
            "daCreated" => $this->arrColumns["daCreated"]["Label"],
            "txShortDesc" => $this->arrColumns["txShortDesc"]["Label"],
            "isActive" => $this->arrColumns["isActive"]["Label"]
        );
        $strSelect = "SELECT * FROM " . $this->dbTable . " WHERE iDeleted = 0 ORDER BY daStart DESC LIMIT " . $Limit;
        return $this->db->_fetch_array($strSelect);
    }

    public function getCato($Limit, $cat, $catname, $andor)
    {
        $this->arrLabels = array(
            "opts" => "Options",
            "vcTitle" => $this->arrColumns["vcTitle"]["Label"],
            "vcImage" => $this->arrColumns["vcImage"]["Label"],
            "vcBtnText" => $this->arrColumns["vcBtnText"]["Label"],
            "daCreated" => $this->arrColumns["daCreated"]["Label"],
            "txShortDesc" => $this->arrColumns["txShortDesc"]["Label"],
            "isActive" => $this->arrColumns["isActive"]["Label"]
        );
        $strSelect = "SELECT * FROM " . $this->dbTable . " WHERE vcType = '" . $cat . "'" . $andor . " vcTypeName = '" . $catname . "' AND iDeleted = 0 ORDER BY daStart DESC LIMIT  " . $Limit;
        return $this->db->_fetch_array($strSelect);
    }

    public function getCatogrys()
    {
        $this->arrLabels = array(
            "opts" => "Options",
        );
        $strSelect = "SELECT vcType, vcTypeName FROM " . $this->dbTable . " WHERE iDeleted = 0 ORDER BY daStart DESC";
        return $this->db->_fetch_array($strSelect);
    }


    public function getEventCount($eventType = NULL, $eventTypeName = NULL)
    {

        $strSelect = "SELECT COUNT($eventType) AS NUM FROM $this->dbTable WHERE $eventType = '$eventTypeName'";
        return $this->db->_fetch_array($strSelect)[0]["NUM"];

    }

    /* public function getAllNews()
     {
         $this->arrLabels = array(
             "opts" => "Options",
             "vcTitle" => $this->arrColumns["vcTitle"]["Label"],
             "daCreated" => $this->arrColumns["daCreated"]["Label"],
             "txShortDesc" => $this->arrColumns["txShortDesc"]["Label"],
             "isActive" => $this->arrColumns["isActive"]["Label"]
         );
         $strSelect = "SELECT * FROM news WHERE iDeleted = 0 ORDER BY iNewsID DESC LIMIT 1";
         return $this->db->_fetch_array($strSelect);
     }*/

    /**
     *
     * @return Object
     */
    public function save()
    {
        return parent::saveItem();
    }

    public function delete($iItemID)
    {
        parent::delete($iItemID);
    }
}
